;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 6


;;;; Exercise 6.5
(setf line `(roses are red))

(reverse line) ; (red are roses)
(first (last line)) ; red/error??? correct: red
(nth 1 line) ; are
(reverse (reverse line)) ; (roses are red)
(append line (list (first line))) ; (roses are red roses)
(append (last line) line) ; (red roses are red)
(list (first line) (last line)) ; (roses red) F! correct: (roses (red))
(cons (last line) line) ; ((red) roses are red)
(remove 'are line) ; (roses red)
(append line '(violets are blue)) ; (roses are red violets are blue)

;;;; Exercise 6.8
(defun my-butlast (l)
  (reverse (nthcdr 1 (reverse l))))

(my-butlast '(roses are red))
(my-butlast '(g a g a))

;;;; Exercise 6.9
(defun palindromep (l)
  (equal l (reverse l)))

(palindromep '(a b c b a))
(palindromep '(a b c c b a))
(palindromep '(a b c a b c))

;;;; Exercise 6.15
(defun contains-the-p (sent)
  (member 'the sent))

(defun article-p (l)
  (intersection l '(the a an)))

(defun article-p (l)
  (or (member 'the l)
      (member 'a l)
      (member 'an l)))

(article-p `(here is an easy test))
(article-p `(here is no test))

;;; Exercise 6.21
(defun my-subsetp (x y)
  (not (set-difference x y)))

(my-subsetp '(a i) '(a e i o u))
(my-subsetp '(a x) '(a e i o u))

;;; Exercise 6.26
;; (a)
(defun right-side (l)
  (remove '-vs- (member '-vs- l)))

(right-side '(large red shiny cube -vs- small shiny red four-sided pyramid))

;; (b)
(defun left-side (l)
  (reverse (right-side (reverse l))))

(left-side '(large red shiny cube -vs- small shiny red four-sided pyramid))

;; (c)
(defun count-common (l)
  (length (remove-duplicates (intersection (left-side l) (right-side l)))))

(count-common '(large red shiny cube -vs- small shiny red four-sided pyramid))

;; (d)
(defun compare (l)
  (list (count-common l) 'common 'features))

(compare '(large red shiny cube -vs- small shiny red four-sided pyramid))
(compare '(small red metal cube -vs- red plastic small cube))

;;; Exercise 6.35
;; (a)
(setf nerd-states '((sleeping eating)
		    (eating waiting)
		    (waiting programming)
		    (programming debugging)
		    (debugging sleeping)))

;; (b)
(defun nerdus (state)
  (second (assoc state nerd-states)))

(nerdus 'sleeping)
(nerdus 'debugging)

;; (d)
(defun sleepless-nerd (state)
  (if (equal state 'debugging)
    'eating
    (nerdus state)))

(sleepless-nerd 'debugging)

;; (e)
(defun nerd-on-caffeine (state)
  (nerdus (nerdus state)))

(nerd-on-caffeine 'eating)
(nerd-on-caffeine 'programming)

;; (f)
(nerd-on-caffeine 'programming)
(nerd-on-caffeine 'sleeping)
(nerd-on-caffeine 'waiting)

;;; Exercise 6.36
(defun swap-first-last (l)
  (append
    (last l)
    (reverse (rest (reverse (rest l))))
    (list (first l))))

(swap-first-last '(you cant buy love))

;;; Exercise 6.37
(defun rotate-left (l)
  (append
    (rest l)
    (list (first l))))

(defun rotate-right (l)
  (append
    (last l)
    (reverse (rest (reverse l)))))

(rotate-left '(a b c d e))
(rotate-right '(a b c d e))

;;; Exercise 6.41
(setf rooms
      '((living-room (north front-stairs)
		     (south dining-room)
		     (east kitchen))
	(upstairs-bedroom (west library)
			  (south front-stairs))
	(dining-room (north living-room)
		     (east pantry)
		     (west downstairs-bedroom))
	(kitchen (west living-room)
		 (south pantry))
	(pantry (north kitchen)
		(west dining-room))
	(downstairs-bedroom (north back-stairs)
			    (east dining-room))
	(back-stairs (south downstairs-bedroom)
		     (north library))
	(front-stairs (north upstairs-bedroom)
		      (south living-room))
	(library (east upstairs-bedroom)
		 (south back-stairs))))

;; (a)
(defun choices (room-name)
  (rest (assoc room-name rooms)))

(choices 'pantry)

;; (b)
(defun look (dir rm)
  (first (rest (assoc dir (choices rm)))))

(look 'north 'pantry)
(look 'west 'pantry)
(look 'south 'pantry)

;; (c)
(setf loc 'pantry)

(defun set-robbie-location (place)
  "Moves Robbie to PLACE by setting the variable LOC."
  (setf loc place))

;; (d)
(defun how-many-choices ()
  (length (choices loc)))

(how-many-choices)

;; (e)
(defun upstairs-p (rm)
  (if (member rm '(library bedroom))
    t
    nil))

(defun onstairs-p (rm)
  (if (member rm '(front-stairs back-stairs))
    t
    nil))

;; (f)
(defun where ()
  (cond ((upstairs-p loc) (list 'robbie 'is 'upstairs 'in 'the loc))
	((onstairs-p loc) (list 'robbie 'is 'on 'the loc))
	(t (list 'robbie 'is 'downstairs 'in 'the loc))))

(where)

;; (g)
(defun move (dir)
  (let* ((rm (look dir loc)))
    (or (and rm
	     (setf loc rm)
	     (where))
	(and (not rm)
	     '(ouch! robbie hit a wall)))))

(move 'south)
(move 'north)

;; (h)
(setf loc 'pantry)
(where)
(move 'west)
(move 'west)
(move 'north)
(move 'north)
(move 'east)
(move 'south)
(move 'south)
(move 'east)
(where)

;;; Exercise 6.42
(defun royal-we (l)
  (subst 'we 'i l))

(royal-we '(if i learn lisp i will be pleased))

