;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 5


;;;; Exercise 5.6

;;; (a)
(defun throw-die ()
  (+ (random 6) 1))

;;; (b)
(defun throw-dice ()
  (list (throw-die) (throw-die)))

(throw-dice)

;;; (c)
(defun snake-eyes-p (thr)
  (equal thr '(1 1)))
(defun boxcars-p (thr)
  (equal thr '(6 6)))

(snake-eyes-p '(1 1))

;;; (d)
(defun instant-win-p (thr)
  (let ((s (+ (first thr) (second thr))))
    (or (= s 7)
	(= s 11))))
(defun instant-loss-p (thr)
  (let ((s (+ (first thr) (second thr))))
    (or (= s 2)
	(= s 3)
	(= s 12))))

(instant-win-p '(5 2))

;;; (e)
(defun say-throw (thr)
  (cond ((snake-eyes-p thr) 'snake-eyes)
	((boxcars-p thr) 'boxcars)
	(t (+ (first thr) (second thr)))))

(say-throw '(3 4))
(say-throw '(1 1))
(say-throw '(6 6))
	
;;; (f)
(defun craps ()
  (let* ((thr (throw-dice))
	 (output1 (list 'throw (first thr) 'and (second thr) '-- (say-throw thr) '--))
	 (output (append output1 (cond ((instant-win-p thr) '(you win))
				       ((instant-loss-p thr) '(you lose))
				       (t (list 'your 'point 'is (+ (first thr) (second thr))))))))
    output))

(defun craps ()
  (let* ((thr (throw-dice))
	 (a (first thr))
	 (b (second thr))
	 (s (+ a b))
	 (outcome (cond ((instant-win-p thr) '(you win))
				       ((instant-loss-p thr) '(you lose))
				       (t (list 'your 'point 'is s)))))
    (append (list 'throw a 'and b '-- (say-throw thr) '--) outcome)))

(craps)

(defun try-for-point (pt)
  (let* ((thr (throw-dice))
	 (a (first thr))
	 (b (second thr))
	 (s (+ a b))
	 (outcome (cond ((equal pt (+ (first thr) (second thr))) '(you win))
				       ((equal 7 (+ (first thr) (second thr))) '(you lose))
				       (t '('try 'again )))))
    (append (list 'throw a 'and b '-- (say-throw thr) '--) outcome)))

(craps)
(try-for-point 9)

