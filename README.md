### GISC exercises
These are my solutions to some of the exercises from David S. Touretzky's book
*Common Lisp: A Gentle Introduction to Symbolic Computation*. You'll probably
need to have a copy of the book at hand to make sense of my code.

This is a simple hobby project with the goal of learning some Common Lisp. It
is poorly maintained and documented.

Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
Licensed under the MIT License; see the file LICENSE for details

