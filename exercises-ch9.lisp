;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 9


;;; Exercise 9.2
(defun draw-line (n)
  (if (> n 0)
    (progn
      (format t "*")
      (draw-line (- n 1)))))

(draw-line 4)

;;; Exercise 9.3
(defun draw-box (cols rows)
  (if (> rows 0)
    (progn
      (draw-line cols)
      (format t "~%")
      (draw-box cols (- rows 1)))))

(draw-box 10 4)

;;; Exercise 9.5
(defun print-board (l)
  (print-board-row l 0)
  (print-board-sep)
  (print-board-row l 3)
  (print-board-sep)
  (print-board-row l 6))

(defun print-board-sep ()
  (format t "-----------~%"))

(defun print-board-row (l pos)
  (format t " ~a | ~a | ~a ~%"
	  (nth pos l)
	  (nth (+ pos 1) l)
	  (nth (+ pos 2) l)))

(defun print-token (todo

(print-board '(X O O NIL X NIL O NIL X))

