;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 7


;;; Exercise 7.7
(defun flip (l)
  (mapcar (lambda (x)
	    (if (equal x 'up)
	      'down
	      'up))
	  l))

(flip '(up down up up))

;;; Exercise 7.8
(defun find-rough-match (l x)
  (find-if (lambda (y)
	     (and (< y (+ x 10))
		  (> y (- x 10))))
	   l))

(find-rough-match '(11 48 60 89) 61)

;;; Exercise 7.10
;; (a)
(setf note-table '((c 1) (c-sharp 2) (d 3) (d-sharp 4) (e 5) (f 6)
		(f-sharp 7) (g 8) (g-sharp 9) (a 10) (a-sharp 11) (b 12)))

;; (b)
(defun numbers (notes)
  (mapcar (lambda (note) (second (assoc note note-table))) notes))

(numbers '(e d c d e e e))

;; (c)
(defun rlookup (val l)
  (first (find-if
	   (lambda (pair)
	     (= val (second pair)))
	   l)))
(defun notes (numbers)
  (mapcar (lambda (n) (rlookup n note-table)) numbers))

(notes '(5 3 1 3 5 5 5))

;; (d)
(numbers (numbers '(e d c))) ; nil...
(notes (notes '(e d c))) ; error

;; (e)
(defun raise (n l)
  (mapcar (lambda (m) (+ n m)) l))

(raise 5 '(5 3 1 3 5 5 5))

;; (f)
(defun normalize (l)
  (mapcar (lambda (n)
	    (cond ((> n 12) (- n 12))
		  ((< n 1) (+ n 12))
		  (t n)))
	  l))

(normalize '(6 10 13))

;; (g)
(defun transpose (n song)
  (notes (normalize (raise n (numbers song)))))

(transpose 5 '(e d c d e e e))
(transpose 11 '(e d c d e e e))
(transpose 12 '(e d c d e e e))
(transpose -1 '(e d c d e e e))


;;; Exercise 7.15
;; (a)
(defun rank (card)
  (first card))
(defun suit (card)
  (second card))

(rank '(2 clubs))
(suit '(2 clubs))

;; (b)
(setf my-hand '((3 hearts)
	       (5 clubs)
	       (2 diamonds)
	       (4 diamonds)
	       (ace spades)))

(defun count-suit (suit hand)
  (length (remove-if-not
	    (lambda (card)
	      (equal suit (suit card)))
	    hand)))

(count-suit 'diamonds my-hand)

;; (c)
(setf colors '((clubs black)
       (diamonds red)
       (hearts red)
       (spades black)))

(defun color-of (card)
  (second (assoc (suit card) colors)))

(color-of '(2 clubs))
(color-of '(6 hearts))

;; (d)
(defun first-red (hand)
  (find-if
    (lambda (card)
      (equal 'red (color-of card)))
    hand))

(first-red my-hand)

;; (e)
(defun black-cards (hand)
  (remove-if-not
    (lambda (card)
      (equal 'black (color-of card)))
    hand))

(black-cards my-hand)

;; (f)
(defun what-ranks (suit hand)
  (mapcar
    #'first
    (remove-if-not
      (lambda (card)
	(equal suit (suit card)))
      hand)))

(what-ranks 'diamonds my-hand)
(what-ranks 'spades my-hand)

;; (g)
(setf all-ranks '(2 3 4 5 6 7 8 9 10 jack queen king ace))

(defun higher-rank-p (card1 card2)
  (member (rank card1) (member (rank card2) all-ranks)))

(higher-rank-p '(queen hearts) '(4 spades))
(higher-rank-p '(queen hearts) '(ace clubs))

;; (h)
(defun high-card1 (hand)
  (let* ((all-ranks-high-to-low (reverse all-ranks))
	 (ranks-in-hand (mapcar #'rank hand))
	 (sorted-ranks-in-hand (remove-if-not
				 (lambda (r)
				   (member r ranks-in-hand))
				 all-ranks-high-to-low))
	 (highest-rank (first sorted-ranks-in-hand)))
    (assoc highest-rank hand)))
(defun high-card2 (hand)
  (reduce
    (lambda (card1 card2)
	    (if (higher-rank-p card1 card2)
	      card1
	      card2))
    hand))

(high-card1 my-hand)
(high-card2 my-hand)

;;; Exercise 7.29
(setf database '((b1 shape brick)
		 (b1 color green)
		 (b1 size small)
		 (b1 supported-by b2)
		 (b1 supported-by b3)
		 (b2 shape brick)
		 (b2 color red)
		 (b2 size small)
		 (b2 supports b1)
		 (b2 left-of b3)
		 (b3 shape brick)
		 (b3 color red)
		 (b3 size small)
		 (b3 supports b1)
		 (b3 right-of b2)
		 (b4 shape pyramid)
		 (b4 color blue)
		 (b4 size large)
		 (b4 supported-by b5)
		 (b5 shape cube)
		 (b5 color green)
		 (b5 size large)
		 (b5 supports b4)
		 (b6 shape brick)
		 (b6 color purple)
		 (b6 size large)))

;; (a)
(defun match-element (s1 s2)
  (or (and (equal s1 s2) t)
      (and (equal s2 '?) t)))

(match-element 'red 'red)
(match-element 'red '?)
(match-element 'red 'blue)

;; (b)
(defun match-triple (ass pat)
  (every #'match-element ass pat))

(match-triple '(b2 color red) '(b2 color ?))
(match-triple '(b2 color red) '(b1 color green))

;; (c)
(defun fetch (pat)
  (remove-if-not
    (lambda (rec)
      (match-triple rec pat))
    database))

(fetch '(b2 color ?))
(fetch '(? supports b1))

;; (d)
(fetch '(b4 shape ?)) ; pyramid
(fetch '(? shape brick)) ; b1, b2, b3, b6
(fetch '(b2 ? b3)) ; left-of
(fetch '(? color ?)) ; ...long list...
(fetch '(b4 ? ?)) ; large blue pyramid supported by B5

;; (e)
(defun make-color-query (blc)
  (list blc 'color '?))

(make-color-query 'b3)

;; (f)
(defun supporters (blc)
  (mapcar
    #'third
    (fetch (list blc 'supported-by '?))))

(supporters 'b1)

;; (g)
(defun supp-cube-p (blc)
  (and
    (find-if
      (lambda (b)
	(fetch (list b 'shape 'cube)))
      (supporters blc))
    t))

(supp-cube-p 'b4)
(supp-cube-p 'b1)

;; (h)
(defun desc1 (blc)
  (fetch (list blc '? '?)))

(desc1 'b6)

;; (i)
(defun desc2 (blc)
  (mapcar #'rest (desc1 blc)))

(desc2 'b6)

;; (j)
(defun description (blc)
  (reduce #'append (desc2 blc)))

(description 'b6)

