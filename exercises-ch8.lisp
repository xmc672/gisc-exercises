;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 8


;;; Exercise 8.5
(defun add-up (l)
  (cond ((null l) 0)
	(t (+ (first l) (add-up (rest l))))))

(add-up '(2 3 7))

;;; Exercise 8.7
(defun all-odd-p (l)
  (cond ((null l) t)
	(t (and
	     (oddp (first l))
	     (all-odd-p (rest l))))))

(all-odd-p '(3 7 19))
(all-odd-p '(3 6 19))

;;; Exercise 8.8
(defun rec-assoc (key l)
  (cond ((null l) nil)
  	((equal (first (first l)) key) (first l))
	(t (rec-assoc key (rest l)))))

(rec-assoc 'c '((a 1) (b 2) (c 3)))

;;; Exercise 8.9
(defun rec-nth (n l)
  (cond ((null l) nil)
	((= n 0) (car l))
	(t (rec-nth (- n 1) (cdr l)))))

(rec-nth 1 '(a b c))

;;; Exercise 8.11
(defun fib (n)
  (cond ((= n 0) 1)
	((= n 1) 1)
	(t (+ (fib (- n 1)) (fib (- n 2))))))

(fib 6)

;;; Exercise 8.14
(defun ir ()
  (list 'ir (ir)))

(ir)

;;; Exercise 8.22
(defun all-equal (l)
  (cond ((< (length l) 2) t)
	((not (equal (first l) (second l))) nil)
	(t (all-equal (rest l)))))

(all-equal '(a a a a))
(all-equal '(a a b a))

;;; Exercise 8.29
(defun my-member (x l)
  (cond ((null l) nil)
	((equal x (first l)) l)
	(t (my-member x (rest l)))))

(my-member 'b '(a b c))
(my-member 'd '(a b c))

;;; Exercise 8.31
(defun compare-lengths (l1 l2)
  (cond ((and
	   (not (cdr l1))
	   (not (cdr l2))) 'same-length)
	((not (cdr l2)) 'first-is-longer)
	((not (cdr l1)) 'second-is-longer)
	(t (compare-lengths (rest l1) (rest l2)))))

(compare-lengths '(a b c) '(x y z))
(compare-lengths '(a b c) '(x y))
(compare-lengths '() '(x y))

;;; Exercise 8.39
(defun count-atoms (tree)
  (cond ((atom tree) 1)
	(t (+
	     (count-atoms (car tree))
	     (count-atoms (cdr tree))))))

(count-atoms '(a (b) c))

;;; Exercise 8.40
(defun count-cons (tree)
  (cond ((atom tree) 0)
	(t (+
	     1
	     (count-cons (car tree))
	     (count-cons (cdr tree))))))

(count-cons '(foo))

;;; Exercise 8.42
(defun my-subst (new old l)
  (my-subst1 new old l nil))
(defun my-subst1 (new-elt old-elt old-list new-list)
  (cond ((null old-list) new-list)
	(t (my-subst1
	     new-elt
	     old-elt
	     (rest old-list)
	     (append
	       new-list
	       (if (equal (first old-list) old-elt)
		 (list new-elt)
		 (list (first old-list))))))))

(my-subst 'x 'c '(a b c d))

;;; Exercise 8.43
(defun flatten (l)
  (cond ((null l) nil)
	(t (append
	     (cond
		((listp (first l)) (flatten (first l)))
		(t (list (first l))))
	  (flatten (rest l))))))

(flatten '((A B (R)) A C (A D ((A (B)) R) A)))

;;; Exercise 8.44
(defun tree-depth (tree)
  (cond ((null tree) 0)
	((atom tree) 0)
	(t (+ 1 (max
		  (tree-depth (first tree))
		  (tree-depth (rest tree)))))))

(tree-depth '(a . b))
(tree-depth '((a b c d)))
(tree-depth '((a . b) . (c . d)))

;;; Exercise 8.45
(defun paren-depth (l)
  (cond ((null l) 0)
	((atom l) 0)
	(t (max
	     (+ 1 (paren-depth (first l)))
	     (+ 0 (paren-depth (rest l)))))))

(paren-depth '(a (x))) ; 2
(paren-depth '(a b c))
(paren-depth '(a b ((c) d) e))

;;; Exercise 8.60
(setf family '((colin nil nil)
	       (deirdre nil nil)
	       (arthur nil nil)
	       (kate nil nil)
	       (frank nil nil)
	       (linda nil nil)
	       (suzanne colin deirdre)
	       (bruce arthur kate)
	       (charles arthur kate)
	       (david arthur kate)
	       (ellen arthur kate)
	       (george frank linda)
	       (hillary frank linda)
	       (andre nil nil)
	       (tamara bruce suzanne)
	       (vincent bruce suzanne)
	       (wanda nil nil)
	       (ivan george ellen)
	       (julie george ellen)
	       (marie george ellen)
	       (nigel andre hillary)
	       (frederick nil tamara)
	       (zelda vincent wanda)
	       (joshua ivan wanda)
	       (quentin nil nil)
	       (robert quentin julie)
	       (olivia nigel marie)
	       (peter nigel marie)
	       (erica nil nil)
	       (yvette robert zelda)
	       (diane peter erica)))

;; (a)
(defun father (x)
  (second (assoc x family)))
(defun mother (x)
  (third (assoc x family)))
(defun parents (x)
  (remove-if
    #'null
    (rest (assoc x family))))
(defun children (x)
  (mapcar
    #'first
    (remove-if-not
      #'(lambda (rec)
	  (and x (member x (rest rec))))
      family)))

(father 'suzanne)
(parents 'suzanne)
(parents 'frederick)
(children 'arthur)
(mother nil)
(parents nil)
(children nil)

;; (b)
(defun siblings (x)
  (mapcar
    #'first
    (remove-if
      #'(lambda (rec)
	  (let ((y (first rec)))
	    (or
	      (equal y x)
	 	(and
	 	  (not (equal (father x) (father y)))
	 	  (not (equal (mother x) (mother y)))))))
      family)))

(siblings 'bruce)
(siblings 'zelda)

;; (c)
(defun mapunion (f l)
  (reduce
    #'union
    (mapcar
      f
      l)))

(mapunion #'rest '((1 a b c) (2 e c j) (3 f a b c d)))

;; (d)
(defun grandparents (x)
  (and
    (parents x)
    (mapunion #'parents (parents x))))

(grandparents 'deirdre)
(grandparents 'yvette)

;; (e)
(defun cousins (x)
  (mapunion
    #'children
    (mapunion
      #'siblings
      (parents x))))

(cousins 'julie)

;; (f)
(defun descended-from (x y)
  (and (not (null x))
       (or
	 (equal x y)
	 (descended-from (father x) y)
	 (descended-from (mother x) y))))

(descended-from 'tamara 'arthur)
(descended-from 'tamara 'linda)

;; (g)
(defun ancestors (x)
  (cond ((null x) nil)
	(t (and (parents x)
		(union
		  (parents x)
		  (mapunion
		    #'ancestors
		    (parents x)))))))

(ancestors 'marie)

;; (h)
(defun generation-gap (x y)
  (cond ((equal x y) 0)
	((null x) nil)
	(t (let* ((gg1 (generation-gap (father x) y))
		 (gg2 (generation-gap (mother x) y))
		 (gg (or gg1 gg2)))
	     (if gg
	       (+ gg 1)
	       nil)))))

(generation-gap 'suzanne 'colin)
(generation-gap 'frederick 'colin)
(generation-gap 'frederick 'linda)

;; (i)
(descended-from 'robert 'deirdre)
(ancestors 'yvette)
(generation-gap 'olivia 'frank)
(cousins 'peter)
(grandparents 'olivia)

;;; Exercise 8.61
(defun tr-count-up (n)
  (trcu1 n nil))
(defun trcu1 (n l)
  (cond ((= n 0) l)
	(t (trcu1 (1- n) (cons n l)))))

(tr-count-up 5)

;;; Exercise 8.62
(defun tr-fact (n)
  (trf n 1))
(defun trf (n result)
  (cond ((= n 0) result)
	(t (trf (1- n) (* n result)))))

(tr-fact 6)

;;; Exercise 8.63
(defun tr-union (a b)
  (cond ((null b) a)
	((member (first b) a) (tr-union a (rest b)))
	(t (tr-union (append a (list (first b))) (rest b)))))

(tr-union '(a b c) '(b d e))

;;; Exercise 8.64
(defun tree-find-if (predicate tree)
  (cond ((null tree) nil)
	((atom tree) (and
		       (funcall predicate tree)
		       tree))
	(t (or
	     (tree-find-if predicate (first tree))
	     (tree-find-if predicate (rest tree))))))

(tree-find-if #'oddp '((2 4) (5 6) 7))

;;; Exercise 8.66
(defun arith-eval (e)
  (cond ((numberp e) e)
	((and (listp e) (= (length e) 3))
	 (funcall
	   (second e)
	   (arith-eval (first e))
	   (arith-eval (third e))))
	(t 'invalid-input)))

(arith-eval '(2 + (3 * 4)))


