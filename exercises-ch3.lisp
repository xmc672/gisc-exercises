;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 3


;;;; Exercise 3.22

;; (c)
(defun myfun (a b)
  (list (list a) b))

(myfun 'alpha 'beta)

;; (d)
(defun firstp (symb l)
  (eql symb (first l)))

(firstp 'foo '(foo bar baz))
(firstp 'boing '(foo bar baz))

;; (e)
(defun mid-add1 (l)
  (list (first l) (+ (second l) 1) (third l)))

(mid-add1 '(take 2 cookies))

;; (f)
(defun F-to-C (x)
  (* 5/9 (- x 32)))

(f-to-c 32)
(f-to-c 59)

;; (g)
(defun foo (x) (+ 1 (zerop x)))

(foo 5)

;;;; Exercise 3.25
(list 'cons t nil) ; (cons t nil)
(eval (list 'cons t nil)) ; (t)
(eval (eval (list 'cons t nil))) ; error

(apply #'cons '(t nil)) ; (t)

(eval nil) ; nil
(list 'eval nil) ; (eval nil)
(eval (list 'eval nil)) ; nil

