;;;; Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
;;;; Licensed under the MIT License; see the file LICENSE for details
;;;;
;;;; A Gentle Introduction to Symbolic Computation
;;;; Chapter 4


;;;; Exercise 4.1
(defun make-even (n)
  (if (evenp n)
    n
    (+ n 1)))

(make-even 1)
(make-even 16)

;;;; Exercise 4.2
;; trivial

;;;; Exercise 4.3
(defun my-not (p)
  (if p
    nil
    t))

(my-not t)
(my-not nil)
(my-not 0)
(my-not 17)

;;; Exercise 4.4
(defun ordered (l)
  (if (> (first l) (second l))
    (reverse l)
    l))

(ordered '(3 4))
(ordered '(4 3))

;;; Exercise 4.10
(defun constrain (x mini maxi)
  (cond ((< x mini) mini)
	((> x maxi) maxi)
	(t x)))

(constrain 3 -50 50)
(constrain 92 -50 50)

;;; Exercise 4.12
(defun cycle (n)
  (cond ((and (> n 0) (< n 99)) (+ n 1))
	((= n 99) 1)
	(t "Invalid input!")))

(cycle 41)
(cycle 99)
(cycle 100)

;;; Exercise 4.19
(and x y z w) ; rewrite using cond; if
(cond ((not x) nil)
      ((not y) nil)
      ((not z) nil)
      ((not w) nil)
      (t w))
(if (not x)
  nil
  (if (not y)
    nil
    (if (not z)
      nil
      (if (not w)
	nil
	w))))

;;; Exercise 4.28
(or (and (oddp 5) (evenp 7)) 'foo)

(if (oddp 5)
  (evenp 7)
  'foo)
(and (evenp 7) (or (and (oddp 5) (evenp 7)) 'foo))

;;; Step
(step (if (oddp 5) 'yes 'no))

